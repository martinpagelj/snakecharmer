<?php 

require_once __DIR__.'/bootstrap/bootstrap.php';
require_once __DIR__.'/config/app.php';

// include login checker
require_once __DIR__.'/login_checker.php';

$page = 'home';
if (isset($_GET['page'])) {
    $page = $_GET['page'];
}

$loggedIn = checkLogin();
$firstname = "";
if($loggedIn == true)
    $firstname = $_SESSION['firstname'];

// Decide which view to render, based on the active page selection
switch ($page) {
    case 'home':
        if($loggedIn == true)
            echo $twig->render('index.twig', ['pageTitle' => $appTitle, 'loggedIn' => $loggedIn, 'firstname' => $firstname] );    
        else 
            header("Location: index.php?page=login");
        break;
    case 'login':
        echo $twig->render('login.twig', ['pageTitle' => $appTitle . ' - Login', 'loggedIn' => $loggedIn]);
        break;
    case 'logout':
        header("Location: " . $homeUrl . "logout");
        break;
    case 'forgotpassword':
        if($loggedIn == false)
            echo $twig->render('forgotpassword.twig', ['pageTitle' => $appTitle . ' - Forgot password', 'loggedIn' => $loggedIn]);
        else 
            header("Location: index.php?page=home");
        break;
    case 'changepassword':
        if($loggedIn == true){
            $userId = $_SESSION['userId'];
            echo $twig->render('changepassword.twig', ['pageTitle' => $appTitle . ' - Change password', 'userId' => $userId, 'loggedIn' => $loggedIn]);
        } else 
            header("Location: index.php?page=login");
        break;
    case 'register':
        $firstname = "";
        $lastname = "";
        $email = "";
        if(isset($_SESSION['registerFormData'])){
            $formData = $_SESSION['registerFormData'];
            $firstname = $formData['firstname'];
            $lastname = $formData['lastname'];
            $email = $formData['email'];
            unset($_SESSION['registerFormData']);
        }
        echo $twig->render('register.twig', ['pageTitle' => $appTitle . ' - Register', 'loggedIn' => $loggedIn, 'firstname' => $firstname, 'lastname' => $lastname, 'email' => $email]);
        break;
    case 'game':
        if($loggedIn == true)
            echo $twig->render('game.twig', ['pageTitle' => $appTitle . ' - List', 'loggedIn' => $loggedIn]);
        else 
            header("Location: index.php?page=login");
        break;
    default: 
        header('HTTP/1.0 404 Not Found');
        echo $twig->render('404.twig');
        break;
}

require_once __DIR__.'/message_handler.php';