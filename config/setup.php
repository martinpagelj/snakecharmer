<?php 
/* 
 * Run this file after installation and database configuration.
 * This will create the 'users' table, which we will use in user sign up and login.
 */

require_once __DIR__.'/database.php';

try {
$db = new Database();
$db = $db->getConnection();

$query = "create table if not exists users 
          (
            id int(6) unsigned auto_increment primary key,
            firstname varchar(50) not null,
            lastname varchar(50) not null,
            email varchar(320) not null unique,
            password char(60) not null,
            created_at timestamp,
            updated_at timestamp
          )";

$db->exec($query);
echo "Table created";
} catch(PDOException $e) {
    echo $e->getMessage();
}