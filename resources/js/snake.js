$(document).ready(function () {
    //Canvas stuff
    var canvas = $("#canvas")[0];
    var ctx = canvas.getContext("2d");
    var w = $("#canvas").width();
    var h = $("#canvas").height();

    //Lets save the cell width in a variable for easy control
    var cw = 10;
    var d;
    var food;
    var score;
    var speedCounter = 0;
    var highscore = 0;
    var keypress = true;
	var startscreenOn = true;
    var firstGame = true;
	var scoreReset = false;
    var pause = false;
    var gameSpeed = 60;

    //Lets create the snake now
    var snake_array; //an array of cells to make up the snake

	// ---------------------------- button events start --------------------------------------------------------
	function resetEvent(){
		ctx.fillStyle = "#3D8BFF";
        ctx.fillRect(w / 2 - 50, 200, 100, 50);

        
        ctx.font = '15px Calibri';
        ctx.fillStyle = 'white';
        ctx.fillText('Reset score', w / 2 - 35, 230);
		canvas.addEventListener("mousedown", resetScoreLogic, false);
	}
	
	function resetScoreLogic(event){
		scoreReset = false;
		var x = event.x;
        var y = event.y;

        x -= canvas.offsetLeft;
        y -= canvas.offsetTop;

        if (x >= 175 && x <= 275 && y >= 200 && y <= 250 && scoreReset != true) {
			resetScore();
			highscore = 0;
			score = 0;
			scoreReset = true;
			ctx.font = '15px Calibri';
			ctx.fillStyle = 'black';
			ctx.fillText('Score was reset', w / 2 - 50, 280);
            canvas.removeEventListener("mousedown", resetScoreLogic, false);
        }
	}
	
	function startScreen() {
		
        ctx.fillStyle = "white";
        ctx.fillRect(0, 0, w, h);
        ctx.strokeStyle = "black";
        ctx.strokeRect(0, 0, w, h);

        ctx.fillStyle = "#3D8BFF";
        ctx.fillRect(w / 2 - 50, 100, 100, 50);

        
        ctx.font = '15px Calibri';
        ctx.fillStyle = 'white';
		if (firstGame == true){
        ctx.fillText('Start game!', w / 2 - 35, 130);
		} else {
		ctx.fillText('Start again!', w / 2 - 35, 130);
		}
		
        if (firstGame != true) {
            var scores = "You got: " + score + " points";

            ctx.font = '15px Calibri';
            ctx.fillStyle = 'blue';
            ctx.fillText(scores, w / 2 - 50, 330);
			
			// main menu button
			
			mainMenuButton();
        }
		
		if (firstGame == true){
		resetEvent();
		}
        canvas.addEventListener("mousedown", startGame, false);

    }
	
	function startGame(event) {
        var x = event.x;
        var y = event.y;

        x -= canvas.offsetLeft;
        y -= canvas.offsetTop;

        if (x >= 175 && x <= 275 && y >= 100 && y <= 150) {
            startscreenOn = false;
            firstGame = false;
			scoreReset = true;
            canvas.removeEventListener("mousedown", startGame, false);
			canvas.removeEventListener("mousedown", resetScoreLogic, false);
            init();
        }
    }
	
    function mainMenuButton(){
    
        ctx.fillStyle = "#3D8BFF";
        ctx.fillRect(w / 2 - 50, 200, 100, 50);

        
        ctx.font = '15px Calibri';
        ctx.fillStyle = 'white';
        ctx.fillText('Main menu', w / 2 - 35, 230);
        canvas.addEventListener("mousedown", mainMenuEvent, false);
    }
	
	function mainMenuEvent(event){
	    var mainMenuClicked = false;
		var x = event.x;
        var y = event.y;

        x -= canvas.offsetLeft;
        y -= canvas.offsetTop;

        if (x >= 175 && x <= 275 && y >= 200 && y <= 250 && mainMenuClicked != true) {
			
			startscreenOn = true;
			firstGame = true;
            canvas.removeEventListener("mousedown", mainMenuEvent, false);
			init();
        }
	}
	
	// ---------------------------- button events end --------------------------------------------------------
	
    function init() {
		highscore = 0;
		var highS = getScore();
        if (highS == null) {
            highscore = 0;
        } else {
            highscore = highS;
        }
		
		if (startscreenOn) {
            keypress = false;
			scoreReset = false;
            startScreen();
			

        } else {
        
        d = "right"; //default direction
        create_snake();
        create_food(); //Now we can see the food particle

        // if (highscore < score) {
            // highscore = score;
            // saveScore(highscore);
        // }

        //finally lets display the score
        score = 0;
        gameSpeed = 60;
        //Lets move the snake now using a timer which will trigger the paint function
        //every 60ms
        if (typeof game_loop != "undefined") clearInterval(game_loop);
        game_loop = setInterval(paint, gameSpeed);
		}
    }
    init();


    function create_snake() {
        var length = 5; //Length of the snake
        snake_array = []; //Empty array to start with
        for (var i = length - 1; i >= 0; i--) {
            //This will create a horizontal snake starting from the top left
            snake_array.push({ x: i, y: 0 });
        }
    }

    //Lets create the food now
    function create_food() {

        x = Math.round(Math.random() * (w - cw) / cw);
        y = Math.round(Math.random() * (h - cw) / cw);



        for (var i = 0; i < snake_array.length; i++) {
            if (x != snake_array[i].x && y != snake_array[i].y) {

                food = { x: x, y: y };
            } else {
                create_food();
            }
        }

        //This will create a cell with x/y between 0-44
        //Because there are 45(450/10) positions accross the rows and columns
    }

    //Lets paint the snake now
    function paint() {
        if (pause == false){
        //To avoid the snake trail we need to paint the BG on every frame
        //Lets paint the canvas now
        ctx.fillStyle = "white";
        ctx.fillRect(0, 0, w, h);
        ctx.strokeStyle = "black";
        ctx.strokeRect(0, 0, w, h);

        
        //The movement code for the snake to come here.
        //The logic is simple
        //Pop out the tail cell and place it infront of the head cell
        var nx = snake_array[0].x;
        var ny = snake_array[0].y;
        //These were the position of the head cell.
        //We will increment it to get the new head position
        //Lets add proper direction based movement now
		
        if (d == "right") nx++;
        else if (d == "left") nx--;
        else if (d == "up") ny--;
        else if (d == "down") ny++;

        //Lets add the game over clauses now
        //This will restart the game if the snake hits the wall
        //Lets add the code for body collision
        //Now if the head of the snake bumps into its body, the game will restart
        if (nx == -1 || nx == w / cw || ny == -1 || ny == h / cw || check_collision(nx, ny, snake_array)) {

			startscreenOn = true;
			
			if (highscore < score) {
                highscore = score;
				saveScore(highscore);
            }
			
            //restart game
            init();

            return;
        }
        
        //Lets write the code to make the snake eat the food
        //The logic is simple
        //If the new head position matches with that of the food,
        //Create a new head instead of moving the tail
        if (nx == food.x && ny == food.y) {
            var tail = { x: nx, y: ny };
            score++;
            //Create new food
            if (highscore < score) {
                highscore = score;
				saveScore(highscore);
            }
            speedCounter++;
            console.log('speedcounter: ' + speedCounter);
            if (speedCounter == 10){

                clearInterval(game_loop);
                gameSpeed = gameSpeed - 1;
                game_loop = setInterval(paint, gameSpeed);
                console.log(gameSpeed);
                speedCounter = 0;
            }
            
            create_food();
        }
        else {
            var tail = snake_array.pop(); //pops out the last cell
            tail.x = nx; tail.y = ny;
        }
        //The snake can now eat the food.

        snake_array.unshift(tail); //puts back the tail as the first cell

        for (var i = 0; i < snake_array.length; i++) {
            var c = snake_array[i];
            //Lets paint 10px wide cells
            paint_cell(c.x, c.y);
        }

        
        keypress = true;
        

        //Lets paint the food
        paint_food_cell(food.x, food.y);
        //Lets paint the score
        var score_text = "Score: " + score;
        ctx.fillText(score_text, 5, h - 5);
        var highscore_text = "Best: " + highscore;
        ctx.fillText(highscore_text, 100, h - 5);
    } else {
        keypress = true;
    }
    }


    //Lets first create a generic function to paint cells
    function paint_cell(x, y) {
        ctx.fillStyle = "blue";
        ctx.fillRect(x * cw, y * cw, cw, cw);
        ctx.strokeStyle = "white";
        ctx.strokeRect(x * cw, y * cw, cw, cw);
    }

    function paint_food_cell(x, y) {
        ctx.fillStyle = "black";
        ctx.fillRect(x * cw, y * cw, cw, cw);
        ctx.strokeStyle = "white";
        ctx.strokeRect(x * cw, y * cw, cw, cw);
    }

    function check_collision(x, y, array) {
        //This function will check if the provided x/y coordinates exist
        //in an array of cells or not
        for (var i = 0; i < array.length; i++) {
            if (array[i].x == x && array[i].y == y)
                return true;
        }
        return false;
    }

    //Lets add the keyboard controls now
    $(document).keydown(function (e) {
        e.preventDefault();
        if (keypress == true && pause == false) {
            var key = e.which;
            //We will add another clause to prevent reverse gear
            if (key == "37" && d != "right") { d = "left"; keypress = false; }
            else if (key == "38" && d != "down") { d = "up"; keypress = false; }
            else if (key == "39" && d != "left") { d = "right"; keypress = false; }
            else if (key == "40" && d != "up") { d = "down"; keypress = false; }
            else if (key == "80") { 
                pause = true;
            }
        } else if (pause == true){
            var key = e.which;
            if (key == "80") pause = false;
        }
        
        //The snake is now keyboard controllable
    });


})

function saveScore(key){
    localStorage.setItem('theHighscore', key);
}

function getScore() {
    var hScore = localStorage.getItem('theHighscore');
    return hScore;
}

function resetScore(){
	localStorage.setItem('theHighscore', 0);
	highscore = 0;
	// OR
	// localStorage.Clear();
}