$(document).ready(function(){
    $('#password, #confirm-password').on('keyup', function () {        
        if ($('#password').val() == $('#confirm-password').val()) {
            $('#message').html('');
        } else 
            $('#message').html('Passwords do not match').css('color', 'red');
    });
})