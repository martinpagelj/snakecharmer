<?php 
require_once dirname(__DIR__, 2).'/config/app.php';
require_once dirname(__DIR__, 2).'/login_checker.php';
if(checkLogin()){
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SnakeCharmer - Snake game</title>
    <script src="/snakecharmer/resources/js/jquery-3.4.1.min.js"></script>
    <script src="/snakecharmer/resources/js/snake.js"></script>
    <link rel="stylesheet" href="/snakecharmer/resources/css/snake.css">
</head>
<body>
    <canvas id="canvas" width="450px" height="450px"></canvas>
</body>
</html>
<?php 
} else {
    header("Location: {$homeUrl}login");
}
?>