<?php

function checkLogin()
{   
    session_start();
    if(isset($_SESSION['loggedIn']) && $_SESSION['loggedIn'] == true) {
        return true;
    }
    return false;
}
