<?php

// Load autoloader
require_once dirname(__DIR__, 1).'/vendor/autoload.php';

// Specify Twig templates location
$loader = new Twig_Loader_Filesystem(dirname(__DIR__, 1).'/resources/templates');

 // Instantiate Twig
$twig = new Twig_Environment($loader);