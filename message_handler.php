<?php
/* 
    TODO: This handler is not perfect, and should probably be changed at some point!
*/


if(isset($_GET['message'])){
    $message = $_GET['message'];
    
    $showAlert = 0;

    if($message == "access-denied" && $_GET['page'] == "login"){  
        $alertType = "alert-danger";
        $message = "Email or password does not exist in our system, please try again!";
        $showAlert = 1;
    } else if($message == "email-error" && $_GET['page'] == "register"){
        $alertType = "alert-danger";
        $message = "Email already exists in our system.";
        $showAlert = 1;
    } else if($message == "wrong-email-error" && $_GET['page'] == "forgotpassword"){
        $alertType = "alert-danger";
        $message = "Email does not exist in our system.";
        $showAlert = 1;
    } else if($message == "recover-password-success" && $_GET['page'] == "login"){
        $alertType = "alert-success";
        $message = "We've sent you an email with a new temporary password.";
        $showAlert = 1;
    } else if($message == "reset-failed-error" && $_GET['page'] == "forgotpassword"){
        $alertType = "alert-danger";
        $message = "Resetting your password failed, please try again.";
        $showAlert = 1;
    } else if($message == "change-password-success" && $_GET['page'] == "changepassword"){
        $alertType = "alert-success";
        $message = "Success! Your password has been changed.";
        $showAlert = 1;
    } else if($message == "change-password-error" && $_GET['page'] == "changepassword"){
        $alertType = "alert-danger";
        $message = "Ooops! Your old password was incorrect, please try again.";
        $showAlert = 1;
    } else if($message == "signup-success" && $_GET['page'] == "login"){
        $alertType = "alert-success";
        $message = "Congratulations on being signed up. You can now login!";
        $showAlert = 1;
    }

    if($showAlert == 1){
        echo '<div class="alert ' . $alertType . ' alert-dismissible fade show" role="alert">' . 
                $message . 
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' . 
                '<span aria-hidden="true">&times;</span>' . 
            '</button>';   
    }
    
}