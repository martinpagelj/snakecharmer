## SnakeCharmer

For live demo, go to http://mpagel.dk/snakecharmer/

### About

This project contains a very basic user creation and login/logout system. 
All of which is created in PHP primarily.

As an added bonus, I've put in a jQuery snake game, which manipulates an HTML5 canvas in order to create the game sprites and movement. 
Do however note that the snake game is _not_ responsive in its design, and it is _not_ mobile or tablet friendly, since it requires a keyboard to be played.

### Technologies used

- PHP v. 7.2.7
- Composer
- Twig
- jQuery v. 3.4.1
- Bootstrap v. 4.3.1
- Fontawesome v. 5.7.1

### Installation

Installing this project is very simple! It requires PHP v. 7.2.7 and Composer.
To download Composer please visit: https://getcomposer.org/download/

- Go to the project root, and run "composer install".
- Next go to config/database.php, change database settings here to match your MySQL database setup.
- Lastly we want to create a "users" table, to automatically do this, run config/setup.php. Remove the setup.php file afterward.
- That's it, you are ready to play! :-) 
