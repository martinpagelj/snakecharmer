<?php 
session_start();

require_once(dirname(__DIR__, 2).'/config/app.php');
require_once(dirname(__DIR__, 2).'/config/database.php');
require_once(dirname(__DIR__, 1).'/User.php');

$db = new Database();
$db = $db->getConnection();

// Initialize model
$user = new User($db);

// Check if user exists in DB
$user->email = $_POST['email'];
$emailExists = $user->emailExists();

if($emailExists && password_verify($_POST['password'], $user->password)){
    $_SESSION['loggedIn'] = true;
    $_SESSION['userId'] = $user->id;
    $_SESSION['firstname'] = $user->firstname;
    $_SESSION['lastname'] = $user->lastname;

    header("Location: {$homeUrl}home");
} else {
    header("Location: {$homeUrl}login&message=access-denied");
}
