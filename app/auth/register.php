<?php

session_start();

require_once(dirname(__DIR__, 2).'/config/app.php');
require_once(dirname(__DIR__, 2).'/config/database.php');
require_once(dirname(__DIR__, 1).'/User.php');

$db = new Database();
$db = $db->getConnection();

// Initialize model
$user = new User($db);

// Set user email to detect if it already exists
$user->email = $_POST['email'];

$error = "";

if($user->emailExists()){

    $error = "email-error";

} else {
    // Fill in new user object
    $user->firstname=$_POST['firstname'];
    $user->lastname=$_POST['lastname'];
    $user->password=$_POST['password'];

    // create user
    if($user->create()){
        header("Location: {$homeUrl}login&message=signup-success");
    } else {
        // User could not be created
        $error = "signup-error";
    }

}

if(!empty($error)){
    $_SESSION['registerFormData'] = [
        'firstname' => htmlspecialchars(strip_tags($_POST['firstname'])),
        'lastname' => htmlspecialchars(strip_tags($_POST['lastname'])),
        'email' => htmlspecialchars(strip_tags($_POST['email']))
    ];
    header("Location: {$homeUrl}register&message={$error}");
}
