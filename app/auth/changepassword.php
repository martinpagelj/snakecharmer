<?php

require_once(dirname(__DIR__, 2).'/config/app.php');
require_once(dirname(__DIR__, 2).'/config/database.php');
require_once(dirname(__DIR__, 2).'/login_checker.php');
require_once(dirname(__DIR__, 1).'/User.php');

if(checkLogin()){

    $db = new Database();
    $db = $db->getConnection();

    // Initialize model
    $user = new User($db);

    // Check if user exists in DB
    $user->id = $_POST['userId'];
    $user->password = $_POST['old-password'];
    $newPassword = $_POST['password'];

    if($user->changePassword($newPassword)){
        header("Location: {$homeUrl}changepassword&message=change-password-success");
    } else {
        header("Location: {$homeUrl}changepassword&message=change-password-error");
    }
} else {
    header("Location: {$homeUrl}login");
}