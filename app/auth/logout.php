<?php 

session_start();

require_once(dirname(__DIR__, 2).'/config/app.php');

unset($_SESSION['loggedIn']);
unset($_SESSION['userId']);
unset($_SESSION['firstname']);
unset($_SESSION['lastname']);

header("Location: {$homeUrl}home");