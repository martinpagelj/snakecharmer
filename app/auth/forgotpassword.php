<?php

require_once(dirname(__DIR__, 2).'/config/app.php');
require_once(dirname(__DIR__, 2).'/config/database.php');
require_once(dirname(__DIR__, 1).'/User.php');

$db = new Database();
$db = $db->getConnection();

// Initialize model
$user = new User($db);

// Check if user exists in DB
$user->email = $_POST['email'];
$emailExists = $user->emailExists();

if($emailExists == true){
    if($user->resetPassword()){
        header("Location: {$homeUrl}login&message=recover-password-success");
    } else {
        header("Location: {$homeUrl}forgotpassword&message=reset-failed-error");
    }

} else {
    header("Location: {$homeUrl}forgotpassword&message=wrong-email-error");
}