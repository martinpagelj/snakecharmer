<?php

class User 
{
    // Database connection and table name
    private $conn;
    private $tableName = "users";

    // User properties
    public $id;
    public $firstname;
    public $lastname;
    public $email;
    public $password;
    public $created_at;
    public $updated_at;

    // Constructor
    public function __construct($db) {
        $this->conn = $db;
    }

    function emailExists(){
        $query = "select id, firstname, lastname, email, password, created_at, updated_at
                  from " . $this->tableName . "
                  where email = :email
                  LIMIT 0,1";
        
        // prepare query
        $stmt = $this->conn->prepare($query);

        // Sanitize, bind email value and finally execute
        $this->email = htmlspecialchars(strip_tags($this->email));
        $stmt->bindParam(':email', $this->email);

        
        $stmt->execute();

        // if email exists, then store the user object
        if($stmt->rowCount() > 0){
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->id = $row['id'];
            $this->firstname = $row['firstname'];
            $this->lastname = $row['lastname'];
            $this->email = $row['email'];
            $this->password = $row['password'];
            $this->created_at = $row['created_at'];
            $this->updated_at = $row['updated_at'];

            return true;
        }
        return false;
    }

    // Creates a new user in system
    function create(){
        $now = date('Y-m-d H:i:s');
        $this->created_at = $now;
        $this->updated_at = $now;

        // Query for new insertion
        $query = "insert into " . $this->tableName . "
                  set 
                    firstname = :firstname,
                    lastname = :lastname,
                    email = :email,
                    password = :password,
                    created_at = :created_at,
                    updated_at = :updated_at";
        
        // Prepare the query
        $stmt = $this->conn->prepare($query);

        // Sanitize
        $this->firstname = htmlspecialchars(strip_tags($this->firstname));
        $this->lastname = htmlspecialchars(strip_tags($this->lastname));
        $this->email = htmlspecialchars(strip_tags($this->email));
        $this->password = htmlspecialchars(strip_tags($this->password));

        // Bind values
        $stmt->bindParam(':firstname', $this->firstname);
        $stmt->bindParam(':lastname', $this->lastname);
        $stmt->bindParam(':email', $this->email);

        // Hash the password before saving to DB
        $passwordHash = password_hash($this->password, PASSWORD_BCRYPT);
        $stmt->bindParam(':password', $passwordHash);

        $stmt->bindParam(':created_at', $this->created_at);
        $stmt->bindParam(':updated_at', $this->updated_at);

        if($stmt->execute()){
            return true;
        } else {
            return false;
        }
    }

    function changePassword($newPassword) {
        // Sanitize input
        $newPassword = htmlspecialchars(strip_tags($newPassword));
        $oldPassword = htmlspecialchars(strip_tags($this->password));
        $userId = htmlspecialchars(strip_tags($this->id));

        $query = "select password from users where id = :userId";
       
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':userId', $userId);
        
        if($stmt->execute()){
            $passwordInDb = $stmt->fetch()['password'];            
            if(password_verify($oldPassword, $passwordInDb)){
                
                $query = "update users 
                          set 
                            password = :password
                          where
                            id = :userId ";

                $stmt = $this->conn->prepare($query);

                $passwordHash = password_hash($newPassword, PASSWORD_BCRYPT);
                $stmt->bindParam(':password', $passwordHash);
                $stmt->bindParam(':userId', $this->id);

                if($stmt->execute()){
                    return true;
                } else { 
                    // if changing password fails, return false
                    return false;
                }

            } else {
                return false;
            }            
        } else {
            // if getting user password fails, return false
            return false;
        }
    }

    function resetPassword() {
        $this->password = $this->randomPassword();
        
        $query = "update users 
                  set 
                    password = :password
                  where
                    id = :userId ";
        
        $stmt = $this->conn->prepare($query);

        // Sanitize input
        $userId = htmlspecialchars(strip_tags($this->id));

        $passwordHash = password_hash($this->password, PASSWORD_BCRYPT);
        $stmt->bindParam(':password', $passwordHash);
        $stmt->bindParam(':userId', $userId);

        if($stmt->execute()){
            
            $to      = $this->email;
            $subject = 'SnakeCharmer - Password recovery';
            $message = "Hi!<br />You have requested a new password. Therefore we'll give you a temporary one, please login and change this.<br /><br />
            Your new password is: {$this->password}";
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= 'From: <no-reply@mpagel.dk>';

            mail($to, $subject, $message, $headers);            
            return true;
        } else {
            return false;
        }

    }

    private function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
}
